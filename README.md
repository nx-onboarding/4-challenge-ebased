# Serverless

## Ejercicio practico: eBased

### Features:

- create-client: adaptar a la libreria eBased
- create-card: adaptar a la libreria eBased
- create-gift: adaptar a la libreria eBased

### Endpoint

- <b>url:</b> https://vjtocgyse6.execute-api.us-east-1.amazonaws.com/dev/client
- <b>method:</b> POST
- <b>body:</b>

```json
{
  "dni": "1234567920",
  "name": "Pedro",
  "lastName": "Pérez",
  "birthDate": "1997-10-01"
}
```

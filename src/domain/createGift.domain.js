const { giftByBirthDate } = require('../helpers');
const { createGiftService } = require('../services/createGift.service');
const { CreateGiftSchema } = require('../schemas/inputs/createGift.schema');


module.exports = async (eventPayload, eventMeta) => {
  const { Message } = eventPayload;
  let client = new CreateGiftSchema(JSON.parse(Message), eventMeta).get();

  client = {
    ...client,
    gift: giftByBirthDate(client.birthDate),
  };

  await createGiftService(client);

  return {
    statusCode: 200,
    body: JSON.stringify({
      client,
      message: 'Card successfully registered',
    }),
  };
};

const { ErrorHandled } = require('ebased/util/error');

const ages = {
  maxAge: 65,
  minAge: 18,
  minAgeForCard: 45,
};

const params = {
  TableName: process.env.CLIENTS_TABLE,
};

const calculateAge = (date) => {
  const today = new Date();
  const birthDate = new Date(date);
  return today.getFullYear() - birthDate.getFullYear();
};

const validateAge = (data) => {
  const age = calculateAge(data.birthDate);
  if (age < ages['minAge'] || age > ages['maxAge']) {
    throw new ErrorHandled(`Must be between ${ages['minAge']} and ${ages['maxAge']} years old`, {
      status: 404,
      code: 'CLIENT_AGE_INVALID',
    });
  }

  return data;
};
const typeCardByAge = (data) => {
  return calculateAge(data.birthDate) < ages['minAgeForCard'] ? 'Classic' : 'Gold';
};

const numberByLength = (length = 16) => {
  return Math.random()
    .toString()
    .substring(2, length + 2);
};

const numberRnd = (minimum, maximum) => {
  const result = Math.round(Math.random() * (maximum - minimum) + minimum);
  return result < 10 ? `0${result}` : result;
};

const giftByBirthDate = (birthday) => {
  const month = Number(birthday.split('-')[1]);
  if (3 <= month && month <= 5) {
    return 'buzo';
  }

  if (6 <= month && month <= 8) {
    return 'sweater';
  }

  if (9 <= month && month <= 11) {
    return 'camisa';
  }

  return 'remera';
};

module.exports = {
  giftByBirthDate,
  numberByLength,
  numberRnd,
  validateAge,
  typeCardByAge,
};

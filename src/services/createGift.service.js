const dynamodb = require('ebased/service/storage/dynamo');

const createGiftService = async (payload, tableName = null) => {
  const { dni, gift } = payload;

  const dbParams = {
    TableName: tableName || process.env.CLIENTS_TABLE,
    ExpressionAttributeNames: {
      '#G': 'gift',
    },
    ExpressionAttributeValues: {
      ':g': gift,
    },
    UpdateExpression: 'SET #G = :g',
    ReturnValues: 'ALL_NEW',
    Key: {
      dni,
    },
  };

  await dynamodb.updateItem(dbParams);
};

module.exports = { createGiftService };

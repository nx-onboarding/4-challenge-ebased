const dynamodb = require('ebased/service/storage/dynamo');
const { ErrorHandled } = require('ebased/util/error');

const existClientService = async (payload, tableName = null) => {
  const { Item } = await dynamodb.getItem({
    TableName: tableName || process.env.CLIENTS_TABLE,
    Key: { dni: payload.dni },
  });

  if (Item) {
    throw new ErrorHandled('Customer already exists', { status: 404, code: 'CLIENT_DUPLICATE' });
  }
};

module.exports = { existClientService };
